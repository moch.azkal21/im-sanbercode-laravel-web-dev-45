<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DaftarController extends Controller
{
    public function daftar()
    {
        return view('page.daftar');
    }

    public function kirim(Request $request)
    {
        $namaDepan = $request->input('fname');
        $namaBelakang = $request->input('lname');

        return view('page.dashboard', ["namaDepan" => $namaDepan, "namaBelakang" => $namaBelakang]);
    }
}
