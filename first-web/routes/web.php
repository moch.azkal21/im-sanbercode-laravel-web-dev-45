<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DaftarController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'home']);
Route::get('/daftar', [DaftarController::class, 'daftar']);

Route::post('/kirim', [DaftarController::class, 'kirim']);

Route::get('/data-tables', function () {
    return view('page.data-tables');
});

//route testing adjusting template

/*Route::get('/master', function () {
    return view('layouts.master');
});*/
